# Front-end Engineering Assessment

The goal is to retrieve and display data provided by the Ontology Lookup Service repository https://www.ebi.ac.uk/ols/index 

You should implement a web application that does the following:

* retrieves EFO term data through the OLS API (https://www.ebi.ac.uk/ols/api/ontologies/efo/terms; check the documentation here: https://www.ebi.ac.uk/ols/docs/api)
* displays a paginated table with selected term data
* displays a bar chart of term label word counts per page (x: word count, y: record count)

* you are free to use any technology stack you like, preferably: Typescript, React, Highcharts, Ant design

There should be clear instructions on how to setup and run the application in a new development environment.

Please provide the solution as a git code repository in your preferred free git hosting service.

The implementation will be evaluated against:

* efficiency of implementation, i.e. minimized response times, re-renders, etc
* use of best practices that promote modularity, code re-use, and maintainability
* code readability and quality

Bonus:
* implement a dropdown that controls the number of items per page
